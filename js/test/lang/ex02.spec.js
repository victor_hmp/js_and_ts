function createUser({ name, email, password, passwordConfirmation }) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }

  return {
    name,
    email,
    password,
    passwordConfirmation
  };
}

const validUser = {
  name: 'Victor Miranda',
  email: 'victor@miranda.com.br',
  password: 'password123',
  passwordConfirmation: 'password123'
};

describe(createUser, () => {
  it('should throw if user name is empty', () => {
    const userWithEmptyName = {
      ...validUser,
      name: ''
    };

    expect(() => createUser(userWithEmptyName)).toThrow(
      'validation error: invalid name size'
    );
  });

  it('should throw if user email has invalid format', () => {
    const userWithInvalidEmail = {
      ...validUser,
      email: 'invalidemail.com'
    };

    expect(() => createUser(userWithInvalidEmail)).toThrow(
      'validation error: invalid email format'
    );
  });

  it('should throw if user password is less than 6 characters', () => {
    const userWithShortPassword = {
      ...validUser,
      password: 'short',
      passwordConfirmation: 'short'
    };

    expect(() => createUser(userWithShortPassword)).toThrow(
      'validation error: invalid password format'
    );
  });

  it('should throw if passwordConfirmation does not match password', () => {
    const userWithShortPassword = {
      ...validUser,
      passwordConfirmation: 'notthesamepassword'
    };

    expect(() => createUser(userWithShortPassword)).toThrow(
      'validation error: confirmation does not match'
    );
  });

  it('should return created user if everything is okay', () => {
    expect(createUser(validUser)).toEqual(validUser);
  });
});
