const test = require('ava');
const scaffoldStructure = require('../../scaffold');

const mockData = [
  { name: 'Fabíola', email: 'fabi@mail.com' },
  { name: 'Bernardo', email: 'b.ern@mail.com' },
  { name: 'Carla', email: 'carl@mail.com' }
];

const expectedListItems = [
  { name: 'Bernardo', email: ' - b.ern@mail.com' },
  { name: 'Carla', email: ' - carl@mail.com' },
  { name: 'Fabíola', email: ' - fabi@mail.com' }
];

test('creates a page with 3 elements containing .name class', t => {
  scaffoldStructure(document, mockData);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, mockData.length);
});

test('creates a page containing an unordered list', t => {
  document.body.innerHTML = '';
  scaffoldStructure(document, mockData);

  const ulNodes = document.querySelector('ul');
  t.truthy(ulNodes);
});

test('creates a page with sorted names', t => {
  document.body.innerHTML = '';
  scaffoldStructure(document, mockData);

  const sortedNames = mockData.map(entry => entry.name).sort();

  const nameNodes = document.querySelectorAll('.name');
  const namesFound = [];

  nameNodes.forEach(node => {
    namesFound.push(node.firstChild.innerHTML);
  });

  t.deepEqual(namesFound, sortedNames);
});

test('creates a page with the expected list item structure', t => {
  document.body.innerHTML = '';
  scaffoldStructure(document, mockData);

  const listNodes = document.querySelectorAll('li');
  const foundNodes = [];

  listNodes.forEach(node => {
    const found = {
      name: node.firstChild.innerHTML,
      email: node.lastChild.nodeValue
    };

    foundNodes.push(found);
  });

  t.deepEqual(foundNodes, expectedListItems);
});
