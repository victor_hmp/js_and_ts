module.exports = function scaffoldStructure(document, data) {
  const ulNode = document.createElement('ul');
  const sortedData = data.sort((a, b) => {
    const nameA = a.name;
    const nameB = b.name;

    if (nameA < nameB) {
      return -1;
    }

    if (nameA > nameB) {
      return 1;
    }

    return 0;
  });

  sortedData.forEach(entry => {
    const nameTextContent = document.createTextNode(entry.name);
    const emailTextContent = document.createTextNode(` - ${entry.email}`);

    const strongNode = document.createElement('strong');
    const liNode = document.createElement('li');

    strongNode.appendChild(nameTextContent);
    liNode.appendChild(strongNode);
    liNode.appendChild(emailTextContent);
    liNode.classList.add('name');

    ulNode.appendChild(liNode);
  });

  document.body.appendChild(ulNode);
};
